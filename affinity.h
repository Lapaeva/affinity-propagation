#ifndef AFFINITY_H
#define AFFINITY_H
#include <gaussgen.h>
#include <vector>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class affinity: public gaussgen
{
public:
    affinity(int row);
    int rownumber;
    Mat data;
    vector<int> cluster;// (count, 0);
    void affinity_propagation (const Mat& data, int lambda);
   // void affinity_propagation(const Mat& similarity, const Mat& preference, int iter, int dampingfactor);
    void affinity_propagation(const Mat& similarity,  int iter, int dampingfactor);
};

#endif // AFFINITY_H
