#include "histogram.h"
#include "math.h"
Histogram::Histogram()
{
}

double Histogram::mancheten(Mat hist1, Mat hist2){
    double dist =0;
    int N=hist1.rows;
    for (int i=0; i<N; i++){
        for (int j=0; j<N; j++){
            dist=dist+abs(hist1.at<double>(i,j)-hist2.at<double>(i,j));
        }
    }
    dist=dist/(N+N);
    return dist;
}

double Histogram::mine(Mat hist1, Mat hist2){
    int N=hist1.rows;
    Mat signVector(1, N+N, CV_64FC1, Scalar(0.0));

    for (int i=0; i<N; i++){
        for (int j=0; j<N; j++){
            signVector.at<double>(0,k)=abs(hist1.at<double>(i,j)-hist2.at<double>(i,j));
        }
    }

    double dist=0;
    for (int i=0; i<N; i++)
    {
        dist=dist+((255/N)*(i+1))*signVector.at<double>(0,i);
    }
    return dist;

}

void Histogram::setHistogramWithHueSaturation(String path){
    Mat src;

    src = imread (path, 1);
    CV_Assert(!src.empty());

    Mat hsv;
    cvtColor(src, hsv, CV_BGR2HSV);
    src.release();

    /// Using 30 bins for hue and 32 for saturation
    int h_bins = 10;
    int s_bins = 10;
    int v_bins = 5;
    int histSize[] = { h_bins, s_bins, v_bins };

    // hue varies from 0 to 256, saturation from 0 to 180
    float h_ranges[] = { 0, 100};
    float s_ranges[] = { 0, 100 };
    float v_ranges[] = {0 ,100};

    const float* ranges[] = { h_ranges, s_ranges, v_ranges };

    // Use the o-th and 1-st channels
    int channels[] = { 0, 1, 2 };

    //hist;
    calcHist( &hsv, 1, channels, Mat(), hist, 2, histSize, ranges, true, false );
    normalize( hist, hist, 0, 1, NORM_MINMAX, -1, Mat() );
}

IplImage* Histogram::compressPic(char* path){
    IplImage* image;
    image = cvLoadImage(path, 1);
    CV_Assert(image!=0);
    IplImage* dst;
    dst= cvCreateImage(cvSize(image->width/13, image->height/13), image->depth, image->nChannels);
    cvResize(image, dst, 0);
    return dst;
}

QImage* Histogram::  IplImage2QImage(IplImage *iplImg)
{
    int h = iplImg->height;
    int w = iplImg->width;
    int channels = iplImg->nChannels;
    QImage *qimg = new QImage(w, h, QImage::Format_ARGB32);
    char *data = iplImg->imageData;

    for (int y = 0; y < h; y++, data += iplImg->widthStep)
    {
        for (int x = 0; x < w; x++)
        {
            char r, g, b, a = 0;
            if (channels == 1)
            {
                r = data[x * channels];
                g = data[x * channels];
                b = data[x * channels];
            }
            else if (channels == 3 || channels == 4)
            {
                r = data[x * channels + 2];
                g = data[x * channels + 1];
                b = data[x * channels];
            }

            if (channels == 4)
            {
                a = data[x * channels + 3];
                qimg->setPixel(x, y, qRgba(r, g, b, a));
            }
            else
            {
                qimg->setPixel(x, y, qRgb(r, g, b));
            }
        }
    }
    return qimg;
}

void Histogram::setHistogram3ChannelsRgb(String path){
    Mat src, rgb;

    src = imread (path, 1);
    CV_Assert(!src.empty());

    cvtColor(src, rgb, CV_BGR2RGB);
   src.release();


    /// using 30 bins for three channels
    int r_bins = 50;
    int g_bins = 50;
    int b_bins = 50;
    int histSize[] = {r_bins,g_bins,b_bins};

    float r_ranges[] = {0, 255};
    float g_ranges[] = {0,255};
    float b_ranges[] = {0, 255};
    const float* ranges[] = {r_ranges,g_ranges,b_ranges};

    bool uniform = true;
    bool accumulate = false;

    const int channels[]={0,1,2};

    //MatND hist;
    calcHist( &rgb, 1, channels, Mat(), hist, 2, histSize, ranges, uniform, accumulate );
    normalize( hist, hist, 0, 1, NORM_MINMAX, -1, Mat() );
}
 void Histogram::setImage(String path){
     char* pathc = new char[path.length()+1];
     strcpy(pathc, path.c_str());
     Mat src = compressPic(pathc);
     imwrite("0"+path, src);
     src.release();
     //img=IplImage2QImage(compressPic(pathc));
 }

 double Histogram::evklid(Mat hist1, Mat hist2){
     double dist=0;
     int N=hist1.cols;
     for(int i=0; i<N; i++) {
         for (int j=0; j<N; j++){
             dist=dist+(hist1.at<double>(i,j)-hist2.at<double>(i,j))*(hist1.at<double>(i,j)-hist2.at<double>(i,j));
         }
     }
    return dist;
 }
 

// QImage* Histogram::visulizationHist(Histogram hist){

//     for( int i = 1; i < histSize; i++ )
//     {
//         line( histImage, Point( bin_w*(i-1), hist_h - cvRound(b_hist.at<float>(i-1)) ) ,
//                          Point( bin_w*(i), hist_h - cvRound(b_hist.at<float>(i)) ),
//                          Scalar( 255, 0, 0), 2, 8, 0  );
//         line( histImage, Point( bin_w*(i-1), hist_h - cvRound(g_hist.at<float>(i-1)) ) ,
//                          Point( bin_w*(i), hist_h - cvRound(g_hist.at<float>(i)) ),
//                          Scalar( 0, 255, 0), 2, 8, 0  );
//         line( histImage, Point( bin_w*(i-1), hist_h - cvRound(r_hist.at<float>(i-1)) ) ,
//                          Point( bin_w*(i), hist_h - cvRound(r_hist.at<float>(i)) ),
//                          Scalar( 0, 0, 255), 2, 8, 0  );
//     }

// }
