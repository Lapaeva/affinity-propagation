#include "k_centers.h"

k_centers::k_centers(int row)
{
    cluster.resize(row, 0);
}

void k_centers::generate_columns(Mat &similarity)
{
    columns.resize(similarity.cols);
    for (int i = 0; i< similarity.cols; i++){
        Column* tmp  = new Column(similarity.col(i),i);
        columns[i] = *tmp;
    }
}

void k_centers::alg(Mat &similarity, int k, int change, int iter)
{
    Mat kvector(1, k, CV_64FC1, Scalar(0,0));

    int N=similarity.rows;
    vector <int> tmp;
    tmp.resize(N,0);

    vector <int> result;
    result.resize(k,0);
    for (int i = 0; i < N; i++){
        tmp[i] = i;
    }
    int n = N;
    for (int i = 0; i< k; i++){
        int ind = rand()%(n-1);
        result[i] =tmp[ind];
        tmp.erase(tmp.begin()+ind);
        n--;
    }
    //*******************************
    //result = centers!!!
    //*******************************
    generate_columns(similarity);
    int num = N/result.size();

    int* nums = new int[result.size()];
    for (int i = 0 ; i <result.size()-1 ; i++) nums[i] = num;
    nums[result.size()-1] = num + N%result.size();
    for (int i = 0 ; i < result.size(); i++){
        for (int j = 0; j < columns.size(); j++){
            vector<element> elements;
            Column tmp = columns[j];
            element* el = new element(j,tmp.getElement(result[i]));
            elements.insert(elements.end(),*el);
            for (int ii = 0; ii< elements.size(); ii++)
                for (int jj = 0; jj<elements.size() - ii; jj++){
                    if (elements.at(jj).value > elements.at(jj+1).value){
                        element temp_el = elements[jj];
                        elements[jj]= elements[jj+1];
                        elements[jj+1] = temp_el;
                    }
                }
            for (int ii = 0; ii<nums[i]; ii++){
                cluster[elements.at(ii).id] = result[i];
                if (ii!=nums[i]-1){
                    for (int jj = ii+1; jj < nums[i]; jj++){
                        if (elements[jj].id > elements[ii].id)elements[jj].id--;
                    }
                }
                columns.erase(columns.begin()+elements.at(ii).id);
            }
        }
    }
}
