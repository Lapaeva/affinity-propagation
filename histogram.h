#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include <opencv2/opencv.hpp>
#include <QImage>

using namespace cv;
using namespace std;
class Histogram
{
public:
    Histogram();
    Mat hist_r;
    Mat hist_g;
    Mat hist_b;
    //MatND hist;
    Mat hist;
    QImage* img;
    void setImage(String path);
    void setHistogramWithHueSaturation(String path);
    void setHistogram3ChannelsRgb(String path);
    double evklid(Mat hist1, Mat hist2);
    double mancheten (Mat hist1, Mat hist2);
    double mine (Mat hist1, Mat hist2);
//   QImage* visulizationHist (Histogram hist);
//   Mat sDistance()
    IplImage* compressPic (char* path);
    QImage*  IplImage2QImage(IplImage *iplImg);
};


#endif // HISTOGRAM_H
