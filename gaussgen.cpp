#include "gaussgen.h"
#include <opencv2/opencv.hpp>


using namespace cv;
using namespace std;

gaussgen::gaussgen()
{
    rng = RNG(time(0));
}

void gaussgen::generation(double a)
{
    mean=a;
    count=rng.uniform (2, 10);
    arrayXY = Mat (count, 2, CV_64FC1, Scalar (0.0));
    Mat arrayX (1, count, CV_64FC1, Scalar(0.0));
    Mat arrayY (1,count, CV_64FC1, Scalar(0.0));

    double stddev=2;
    randn (arrayX, mean, stddev);
    randn (arrayY, mean, stddev);

    for (int i=0; i<count; i++)
    {
        arrayXY.at<double>(i,0)=arrayX.at<double>(0,i);
        arrayXY.at<double>(i,1)=arrayY.at<double>(0,i);
    }
}
