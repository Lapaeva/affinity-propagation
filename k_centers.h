#ifndef K_CENTERS_H
#define K_CENTERS_H
#include <vector>

#include <opencv2/opencv.hpp>
#include "column.h"
#include "element.h"

using namespace std;
using namespace cv;

class k_centers
{
public:
    k_centers(int row);
    vector<int> cluster;
    vector<Column> columns;
    void alg (Mat &similarity , int k, int change, int iter);
    void generate_columns(Mat &similarity);
};

#endif // K_CENTERS_H
