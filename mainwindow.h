#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPainter>
#include <affinity.h>
#include <gaussgen.h>
#include <histogram.h>
#include <string>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

protected:
    void paintEvent(QPaintEvent *);

public slots:
    void on_gaussSolver_clicked();
private slots:
    void on_affinitySolver_clicked();
    void on_kcentersSolver_clicked();
    void on_random_clicked();

};

#endif // MAINWINDOW_H
