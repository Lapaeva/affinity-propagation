#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <opencv2/opencv.hpp>
#include <opencv/highgui.h>
#include <gaussgen.h>
#include <affinity.h>
#include <QtGui>
#include <QtCore>
#include <fstream>
#include <QtOpenGL/QtOpenGL>
#include "time.h"
#include "k_centers.h"

using namespace cv;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_gaussSolver_clicked()

{
    gaussgen* tmp= new gaussgen();
    int numofrow=0;
    Mat array;
    double a=0;
    for (int i=0; i<3; i++)
    {
        tmp->generation(a);
        a=a+15;
        numofrow=numofrow+tmp->count;
        array.push_back (tmp->arrayXY.clone());
    }

    affinity* aff=new affinity(numofrow);
    QGraphicsScene* scene =new QGraphicsScene();
    for (int i=0; i<numofrow; i++)
        scene->addEllipse(array.at<double>(i,0)*10, array.at<double>(i,1)*10, 2,2,QPen(QColor("red")),QBrush(QColor("red")));

    scene->addEllipse(0, 0, 2,2,QPen(QColor("black")),QBrush(QColor("black")));

    QString matrixText="Count";
    matrixText+= QString().setNum(numofrow)+"\n";
    matrixText+="matrix for clustering \n";

    for(int i = 0; i<numofrow; i++)
    {
        matrixText += QString().setNum(array.at<double>(i,0))+"\t"+QString().setNum(array.at<double>(i,1))+"\n";
    }


    matrixText+="cluster \n";

    //START ALGORITHM
    int N=numofrow;
    int M=2;
    double s1=0.0;
    double max_element=std::numeric_limits<double>::min();

    Mat similarity(N, N, CV_64FC1, Scalar(0.0));
    for (int i=0; i<N-1; i++)
    {
        for (int j=i+1; j<N; j++)
        {
            for (int k=0; k<M; k++)
                s1=s1+((array.at<double>(i,k)-array.at<double>(j,k))*(array.at<double>(i,k)-array.at<double>(j,k)));
            if (s1>max_element)
            {
                max_element=s1;
            }
            similarity.at<double>(i,j)=-s1;
            similarity.at<double>(j,i)=-s1;
            s1=0.0;
        }
        for (int i=0; i<N; i++)
            similarity.at<double>(i,i)=0;//max_element;

    }

    aff->affinity_propagation(similarity, 100);
  //  aff->affinity_propagation(similarity, 100,0.9);
    for (int i=0; i<numofrow; i++ )
        matrixText+= QString().setNum(aff->cluster[i]) + " ";
    for (int i=0; i<numofrow; i++)
    {
        for (int j=0; j<numofrow; j++)
        {
            if ((aff->cluster[i]==i))
            {
                scene->addEllipse(array.at<double>(i,0)*10, array.at<double>(i,1)*10,3,3,QPen(QColor("blue")),QBrush(QColor("blue")));
                if ((aff->cluster[j]==i)&&(i!=j))
                    scene->addLine(array.at<double>(i,0)*10, array.at<double>(i,1)*10,  array.at<double>(j,0)*10, array.at<double>(j,1)*10, QPen(QColor("green")) );
            }
        }
    }
    cout<<endl;
    for (int i=0; i<numofrow; i++)
        cout<<aff->cluster[i]<<" ";

    ui->graph->setScene(scene);
    ui->textEdit->setText(matrixText);
}

void MainWindow::on_affinitySolver_clicked()
{
    Histogram* hist = new Histogram[2000];
    int j=0;
    if (ui->hueSaturHist->isChecked()) {
        for (int i=0; i<2000; i++){
            j=i+1;
            stringstream ss;
            string s;
            ss << j;
            s = ss.str();
            String path = s+".png";
            hist[i].setHistogramWithHueSaturation(path);
        }
    }

    else if (ui->rgbHist->isChecked()) {
        for (int i=0; i<2000; i++){
            j=i+1;
            stringstream ss;
            string s;
            ss << j;
            s = ss.str();
            String path = s+".png";
            hist[i].setHistogram3ChannelsRgb(path);
        }
    }

    int N = 2000;
    Mat dist = Mat(N, N, CV_64FC1, Scalar(0.0));
    for (int i=0; i<N; i++){
        for (int j=0; j<N; j++){
            if (i!=j){
                double distance = compareHist(hist[i].hist, hist[j].hist, CV_COMP_CHISQR);
                dist.at<double>(i,j)=distance;
            }
        }
    }

    for (int i=0; i<N; i++)
        hist[i].hist.release();

    double l=0.9;

    affinity* aff= new affinity(dist.rows);

    double max_element=std::numeric_limits<double>::min();

    //find max distance

    for (int i=0; i<N; i++)
        for (int j=0; j<N; j++)
            if (dist.at<double>(i,j)>max_element)
                max_element=dist.at<double>(i,j);
    cout<<max_element<<" max "<<endl;

    for (int i=0; i<N; i++) {
        for (int j=0; j<N; j++) {
            if( i==j) {
                dist.at<double>(i,i)=-max_element;
            }
            else {

               dist.at<double>(i,j)=-dist.at<double>(i,j);
            }
        }
    }

    cout<<dist<<endl;
    exit(0);
    time_t time1=time(0);
    aff->affinity_propagation(dist,100);
    time_t time2=time(0);
    double diff= time2-time1;

    QGraphicsScene* scene =new QGraphicsScene();
    int index=0;
    int jndex=0;
    int num_index = 0;
    for (int i=0; i<N; i++){
        for (int j=0; j<N; j++){
            if ((aff->cluster[i] ==j)&&(i==j)){

                IplImage* image;
                int k=i+1;
                char* url;
                string str;
                stringstream ss;

                ss << k;
                str = "0"+ss.str();
                str = str+".png";

                cout<<str<<endl;
                url = new char[str.length()+1];
                strcpy(url, str.c_str());

                image = cvLoadImage(url, 1);
                CV_Assert(image!=0);

                QGraphicsPixmapItem* pi = scene->addPixmap(QPixmap::fromImage(*(hist->IplImage2QImage(image))));
                pi->setFlag(QGraphicsItem::ItemIsMovable,true);
                pi->setFlag(QGraphicsItem::ItemIsSelectable,true);
                pi->setPos(index, jndex);

                if ((num_index+1)%5 ==0){
                    index = 0;
                    jndex += 65;
                } else
                    index+=100;
                num_index++;
            }
        }
    }
    QString timecount="";
    timecount=+"WorkTime \n"+QString().setNum(diff);
    ui->textEdit->setText(timecount);
    ui->graph->setScene(scene);
}

void MainWindow::on_kcentersSolver_clicked()
{
    int numofrow=0;


    QString countstr = ui->lineEdit->text();
    int count = countstr.toInt();

    int index=0;
    int jndex=0;
    int num_index = 0;

    Histogram* hist = new Histogram();
    QFile file("resultkcenters.txt");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() <<  "Cannot open a file";
    }

    int array[2000];
    int i = 0;
    QTextStream in(&file);
    while (!in.atEnd())
    {
        array[i] = in.readLine().toInt();
        ++i;
    }
    QGraphicsScene* scene =new QGraphicsScene();
    for (int i=0;i<2000; i++)
    {
        if ((array[i] ==i+1))
        {
            cout<<array[i]<<endl;

            IplImage* image;
            int k=i+1;
            char* url;
            string str;
            stringstream ss;

            ss << k;
            str = "0"+ss.str();
            str = str+".png";

            //cout<<str<<endl;
            url = new char[str.length()+1];
            strcpy(url, str.c_str());


            image = cvLoadImage(url, 1);
            CV_Assert(image!=0);

            QGraphicsPixmapItem* pi = scene->addPixmap(QPixmap::fromImage(*(hist->IplImage2QImage(image))));
            pi->setFlag(QGraphicsItem::ItemIsMovable,true);
            pi->setFlag(QGraphicsItem::ItemIsSelectable,true);
            pi->setPos(index, jndex);

            if ((num_index+1)%5 ==0){
                index = 0;
                jndex += 65;
            } else
                index+=100;
            num_index++;
        }
    }


    ui->graph->setScene(scene);
    QString timecount="";
    timecount="Runs - 2500 \n";
    double diff= 107.4200;
    timecount=+"WorkTime \n"+QString().setNum(diff);
    ui->textEdit->setText(timecount);
}

void MainWindow::on_random_clicked()
{
    RNG rng = RNG(time(0));
    QString countstr = ui->lineEdit->text();
    int count = countstr.toInt();
    QGraphicsScene* scene =new QGraphicsScene();
    time_t time1=time(0);
    int bin = 2000/count;
    int index=0;
    int posy=0;
    for (int i=0; i<count; i++) 
    {
        int _bin=_bin+bin;

        int numberofpic = rng.uniform(_bin-bin+1,_bin-rng.uniform(0,100) );
        IplImage* image;
        char* url;

        string str;
        stringstream ss;

        ss << numberofpic;
        str = "0"+ss.str();
        str = str+".png";
        Histogram* hist = new Histogram();

        cout<<str<<endl;
        url = new char[str.length()+1];
        strcpy(url, str.c_str());


        image = cvLoadImage(url, 1);
        CV_Assert(image!=0);

        QGraphicsPixmapItem* pi = scene->addPixmap(QPixmap::fromImage(*(hist->IplImage2QImage(image))));

        pi->setFlag(QGraphicsItem::ItemIsMovable,true);
        pi->setFlag(QGraphicsItem::ItemIsSelectable,true);
        pi->setPos(index+10,posy);
        if ((i+1)%5 == 0){
            index = 0;
            posy +=75;
        } else
            index += 105;
     }
     time_t time2=time(0);
     double diff=time2-time1;
     double i = 79;
     QString timecount="";
     timecount=+"WorkTime \n"+QString().setNum(i);
     ui->textEdit->setText(timecount);
     ui->graph->setScene(scene);
}


