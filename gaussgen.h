#ifndef GAUSSGEN_H
#define GAUSSGEN_H
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <math.h>

#include <opencv2/opencv.hpp>
//#include <opencv/cvcompat.h>

using namespace std;
using namespace cv;

class gaussgen
{

public:
    int count;
    Mat arrayXY;
    gaussgen();
    void generation (double a);

 private:
    cv::RNG rng;
    double mean;
};

#endif // GAUSSGEN_H
