#include "affinity.h"

affinity::affinity(int row)
{
    rownumber=row;
    cluster.resize(row, 0);
}
void affinity::affinity_propagation(const Mat &similarity, int iter, int dampingfactor)
{
    int N = similarity.rows;
//    Mat similarity(N, N, CV_64FC1, Scalar(0.0));
//    vector<double> pref(N, 0.0);
//    double max_element=std::numeric_limits<double>::min();

//    //find max distance

//    for (int i=0; i<N; i++)
//        for (int j=0; j<N; j++)
//            if (distance.at<double>(i,j)>max_element)
//                max_element=distance.at<double>(i,j);

//    for (int i=0; i<N; i++)
//        pref[i]=max_element;

//    for (int i=0; i<N; i++) {
//        for (int j=0; j<N; j++) {
//            if( i==j) {
//                similarity.at<double>(i,i)=-pref[i];
//            }
//            else {
//                similarity.at<double>(i,j)=-distance.at<double>(i,j);
//            }
//        }
//    }
//    cout<<"simularity"<<endl;
//    cout<<similarity<<endl;


    Mat responsibility (N,N, CV_64FC1, Scalar(0.0));
    Mat availability(N, N, CV_64FC1, Scalar(0.0));
    Mat criterion (N,N, CV_64FC1, Scalar(0.0));
    Mat update (N, N, CV_64FC1, Scalar(0.0));

    //Start iterations
    for (int r=0; r<iter; r++)  // iteration
    {
        vector<double> max_element_in_row (N, std::numeric_limits<double>::min());
        vector<double> next_max_element_in_row (N, std::numeric_limits<double>::min());
        vector<int> index_coloumn_max (N,0);

        int index_column=0;

        // UPDATE AVAILABILITY

        for (int i=0; i<N; i++)
            for (int j=0; j<N; j++)
                update.at<double>(i,j)=availability.at<double>(i,j)+similarity.at<double>(i,j);

        // RESPONSIBILITIES

        Mat old_responsibilities (N,N, CV_64FC1, Scalar(0.0));
        old_responsibilities=responsibility.clone();

        double max_element=-std::numeric_limits<double>::max();
        for (int i=0; i<N; i++)
        {
            for (int j=0; j<N; j++)
                if (update.at<double>(i,j)>max_element)
                {
                    max_element=update.at<double>(i,j);
                    index_column=j;
                }

            index_coloumn_max[i]=index_column;
            max_element_in_row[i]=max_element;
            max_element=-std::numeric_limits<double>::max();

            //search for next max element

            for (int g=0; g<N; g++)
                if ((update.at<double>(i,g)>max_element)&&(index_column!=g))  //&&(update.at<double>(i,g)!=max_element_in_row[i]))
                    max_element=update.at<double>(i,g);

            next_max_element_in_row[i]=max_element;
            max_element=-std::numeric_limits<double>::max();

        }

        for (int i=0; i<N; i++)
            for (int j=0; j<N; j++)
                if (j==index_coloumn_max[i])
                    responsibility.at<double>(i, j)=similarity.at<double>(i,j)-next_max_element_in_row[i];
                else
                    responsibility.at<double>(i,j)=similarity.at<double>(i,j)-max_element_in_row[i];

        //Damping responsibility

        for (int i=0; i<N; i++)
            for (int j=0; j<N; j++)
                responsibility.at<double>(i,j)=(((1-dampingfactor)*responsibility.at<double>(i,j)))+(dampingfactor*old_responsibilities.at<double>(i,j));

        // AVAILABILITY

        Mat old_availabilities (N,N, CV_64FC1, Scalar(0.0));
        old_availabilities=availability.clone();

        // clean matrix

        for (int i=0; i<N; i++)
            for (int j=0; j<N; j++)
                availability.at<double>(i,j)=0;

        for (int i=0; i<N; i++)
            for (int j=0; j<N; j++)
                if ((responsibility.at<double>(j,i)>0))
                    availability.at<double>(i,i)=availability.at<double>(i,i)+responsibility.at<double>(j,i);

        for (int i=0; i<N; i++)
            for (int g=0; g<N; g++)
                if (i!=g){
                    availability.at<double>(g,i)=availability.at<double>(i,i) +responsibility.at<double>(i,i);
                    if (responsibility.at<double>(g,i)>0)
                        availability.at<double>(g,i)=availability.at<double>(g,i)-responsibility.at<double>(g,i);
                }

        for (int i=0; i<N; i++)
            for (int j=0; j<N; j++)
                if (i!=j)
                    if (availability.at<double>(i,j)>0)
                        availability.at<double>(i,j)=0;

        //Damping availability

        for (int i=0;i<N; i++)
            for (int j=0; j<N; j++)
                availability.at<double>(i,j)=(1-dampingfactor)*availability.at<double>(i,j)+dampingfactor*old_availabilities.at<double>(i,j);

    }   // end of iteration cicle

    // CRITERION

    cout<<"Criterion"<<endl;
    for (int i=0; i<N; i++)
    {
        for (int j=0; j<N; j++)
        {
            criterion.at<double>(i,j)= availability.at<double>(i,j)+responsibility.at<double>(i,j);
            criterion.at<double>(i,j) = criterion.at<double>(i,j)*100/100;
        }
    };

    //search for cluster
    double searchforcriterion=-std::numeric_limits<double>::max();
    for (int i=0; i<N; i++)
    {
        for (int j=0; j<N; j++)
            if (criterion.at<double>(i,j)>searchforcriterion)
            {
                searchforcriterion=criterion.at<double>(i,j);
                cluster[i]=j;
            }
        searchforcriterion=-std::numeric_limits<double>::max();
    }
}

void affinity::affinity_propagation(const Mat &similarity, int lambda)
{
    int N=rownumber;
    int M=2;
    double s1=0.0;
    double max_element=std::numeric_limits<double>::min();

   //Mat similarity(N, N, CV_64FC1, Scalar(0.0));
    Mat responsibility (N,N, CV_64FC1, Scalar(0.0));
    Mat availability(N, N, CV_64FC1, Scalar(0.0));
    Mat criterion (N,N, CV_64FC1, Scalar(0.0));
    Mat update (N, N, CV_64FC1, Scalar(0.0));

    // SIMILARITY

//    for (int i=0; i<N-1; i++)
//    {
//        for (int j=i+1; j<N; j++)
//        {
//            for (int k=0; k<M; k++)
//                s1=s1+((data.at<double>(i,k)-data.at<double>(j,k))*(data.at<double>(i,k)-data.at<double>(j,k)));
//            if (s1>max_element)
//            {
//                max_element=s1;
//            }
//            similarity.at<double>(i,j)=-s1;
//            similarity.at<double>(j,i)=-s1;
//            s1=0.0;
//        }
//        for (int i=0; i<N; i++)
//            similarity.at<double>(i,i)=-max_element;
//    }

//    cout<<" similarity "<<endl;
//    for (int i=0; i<N; i++)
//    {
//        for (int j=0; j<N; j++)
//        {
//            cout<<similarity.at<double>(i,j)<<" ";
//        }
//        cout<<";"<<endl;
//    }

    double lam;
    lam=0.9;

    //Start iterations
    for (int r=0; r<lambda; r++)  // iteration
    {
        vector<double> max_element_in_row (N, std::numeric_limits<double>::min());
        vector<double> next_max_element_in_row (N, std::numeric_limits<double>::min());
        vector<int> index_coloumn_max (N,0);

        int index_column=0;

        // UPDATE AVAILABILITY

        for (int i=0; i<N; i++)
            for (int j=0; j<N; j++)
                update.at<double>(i,j)=availability.at<double>(i,j)+similarity.at<double>(i,j);

        // RESPONIBILITIES

        Mat old_responsibilities (N,N, CV_64FC1, Scalar(0.0));
        old_responsibilities=responsibility.clone();

        max_element=-std::numeric_limits<double>::max();
        for (int i=0; i<N; i++)
        {
            for (int j=0; j<N; j++)
                if (update.at<double>(i,j)>max_element)
                {
                    max_element=update.at<double>(i,j);
                    index_column=j;
                }

            index_coloumn_max[i]=index_column;
            max_element_in_row[i]=max_element;
            max_element=-std::numeric_limits<double>::max();

        // search for next max element

            for (int g=0; g<N; g++)
                if ((update.at<double>(i,g)>max_element)&&(index_column!=g))  //&&(update.at<double>(i,g)!=max_element_in_row[i]))
                    max_element=update.at<double>(i,g);

            next_max_element_in_row[i]=max_element;
            max_element=-std::numeric_limits<double>::max();

        }

        for (int i=0; i<N; i++) {
             for (int j=0; j<N; j++) {
                if (j==index_coloumn_max[i])
                    responsibility.at<double>(i, j)=similarity.at<double>(i,j)-next_max_element_in_row[i];
                else
                    responsibility.at<double>(i,j)=similarity.at<double>(i,j)-max_element_in_row[i];
             }
        }

        //Damping responsibility

       for (int i=0; i<N; i++)
            for (int j=0; j<N; j++)
                responsibility.at<double>(i,j)=(((1-lam)*responsibility.at<double>(i,j)))+(lam*old_responsibilities.at<double>(i,j));

        // AVAILABILITY

        Mat old_availabilities (N,N, CV_64FC1, Scalar(0.0));
        old_availabilities=availability.clone();

        // clean matrix

        for (int i=0; i<N; i++)
            for (int j=0; j<N; j++)
                availability.at<double>(i,j)=0;


        for (int i=0; i<N; i++)
            for (int j=0; j<N; j++)
                if ((responsibility.at<double>(j,i)>0))
                    availability.at<double>(i,i)=availability.at<double>(i,i)+responsibility.at<double>(j,i);

        for (int i=0; i<N; i++)
            for (int g=0; g<N; g++)
                if (i!=g){
                    availability.at<double>(g,i)=availability.at<double>(i,i) +responsibility.at<double>(i,i);
                    if (responsibility.at<double>(g,i)>0) {
                        availability.at<double>(g,i)=availability.at<double>(g,i)-responsibility.at<double>(g,i);
                    }
                }

        for (int i=0; i<N; i++){
            for (int j=0; j<N; j++) {
                if (i!=j) {
                    if (availability.at<double>(i,j)>0)
                        availability.at<double>(i,j)=0;

                }
            }
        }

        //Damping availability

        for (int i=0;i<N; i++)
            for (int j=0; j<N; j++)
                 availability.at<double>(i,j)=(1-lam)*availability.at<double>(i,j)+lam*old_availabilities.at<double>(i,j);

    }   // end of iteration cicle

    // criterion
    for (int i=0; i<N; i++)
    {
        for (int j=0; j<N; j++)
        {
            criterion.at<double>(i,j)= availability.at<double>(i,j)+responsibility.at<double>(i,j);
        }
    };

//search for cluster
    double searchforcriterion=-std::numeric_limits<double>::max();
    for (int i=0; i<N; i++)
    {
        for (int j=0; j<N; j++)
        {
            if (criterion.at<double>(i,j)>searchforcriterion)
            {
                searchforcriterion=criterion.at<double>(i,j);
                cluster[i]=j;
            }
        }
        searchforcriterion=-std::numeric_limits<double>::max();
    }
}
