The Affinity Propagation algorithm was established in 2007 by Brendan J. Frey
and Delbert Dueck, who deal with issues of machine learning for long period 
of time. It is a new approach to clustering, based on the exchange of messages 
between objects.  However, as stated by its creators, the algorithm can obtain 
accurate results in a short period of time without additional apriority 
parameters.  

Article: http://www.psi.toronto.edu/affinitypropagation/FreyDueckScience07.pdf

This program have also a comparative analysis between two methods of clustering.   
For comparison was selected long-proven method, called k-means. This is the 
heuristic method, which can quickly handle large amounts of data,  
but it requires an a priori set the number of clusters. 

In this work was used its modification, called k-centers. In the k-centers into 
centers selected objects from a set of data, thus it can be applied in cases, 
where it is impossible to calculate the average over the objects. 

It has been decided to try these algorithms on finding key frames of the video.  
As input of the algorithms was taken distance matrix between frames, computed by 
using different types of histograms. Affinity propagation found key frames with 
much lower error than other method, and it did so in less amount of time.  

Algorithms also can be compared on random generated points.
 