#-------------------------------------------------
#
# Project created by QtCreator 2011-11-17T23:17:49
#
#-------------------------------------------------

QT       += core gui

TARGET    = gauss_gui
TEMPLATE  = app


SOURCES  += main.cpp\
        mainwindow.cpp \
        gaussgen.cpp \
        affinity.cpp \
    histogram.cpp \
    k_centers.cpp \
    element.cpp \
    column.cpp


HEADERS  += mainwindow.h \
            gaussgen.h \
            affinity.h \
    histogram.h \
    k_centers.h \
    element.h \
    column.h

FORMS    += mainwindow.ui

LIBS += -L/usr/local/lib/ \
         -lopencv_calib3d\
        -lopencv_contrib\
        -lopencv_core\
        -lopencv_features2d\
        -lopencv_flann\
        -lopencv_gpu\
        -lopencv_highgui\
        -lopencv_imgproc\
        -lopencv_legacy\
        -lopencv_ml\
        -lopencv_objdetect\
        -lopencv_ts\
        -lopencv_video


INCLUDEPATH += /usr/local/include
